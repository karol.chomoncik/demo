package com.example.demo;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class BookService {

    List<Book> bookList = new ArrayList<>();

    public BookService() {
        bookList.addAll(Arrays.asList(new Book("Main Kampf", "Adolf"),
                new Book("Nazwa", "Autor"))
        );
    }

    public List<Book> getBooks() {
        return bookList;
    }
}
